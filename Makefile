## Run test
test: cog
	tox


## Build document
doc: cog
	make -C doc html


## Update files using cog.py
cog: orgmodify/__init__.py
orgmodify/__init__.py: README.rst
	cd orgmodify && cog.py -r __init__.py
