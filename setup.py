# see https://github.com/karlicoss/pymplate for up-to-date reference
#
from setuptools import setup, find_namespace_packages # type: ignore


def main():
    pkg = 'orgmodify'
    subpkgs = find_namespace_packages('.', include=(pkg + '.*',))

    import orgmodify
    setup(
        name=pkg,
        use_scm_version={
            'version_scheme': 'python-simplified-semver',
            'local_scheme': 'dirty-tag',
        },
        setup_requires=['setuptools_scm'],

        zip_safe=False,

        packages=[pkg, *subpkgs],
        package_data={
            pkg: ['py.typed'], # todo need the rest as well??
            'orgmodify.tests.data': ['*.org'],
        },

        author=orgmodify.__author__,
        author_email='aka.tkf@gmail.com',
        maintainer='Andy Drop (@DrOps@chaos.social)',
        maintainer_email='orgmodify@plaindrops.de',

        url='https://codeberg.org/dr.ops/orgmodify',
        license=orgmodify.__license__,

        description='orgmodify - Emacs org-mode modifier in Python',
        long_description=orgmodify.__doc__,

        keywords='org org-mode emacs',
        classifiers=[
            'Development Status :: 5 - Production/Stable',
            'License :: OSI Approved :: BSD License',
            'Topic :: Text Processing :: Markup',
            # see: http://pypi.python.org/pypi?%3Aaction=list_classifiers
        ],

        extras_require={
            'testing': ['pytest'],
            'linting': ['pytest', 'mypy', 'lxml'], # lxml for mypy coverage report
        },
    )


if __name__ == '__main__':
    main()
