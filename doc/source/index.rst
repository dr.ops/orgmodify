.. orgmodify documentation master file, created by
   sphinx-quickstart on Sun Mar  4 22:50:33 2012.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. automodule:: orgmodify


Tree structure interface
========================

.. py:module:: orgmodify.node

.. inheritance-diagram::
   orgmodify.node.OrgBaseNode
   orgmodify.node.OrgRootNode
   orgmodify.node.OrgNode
   :parts: 1

.. autoclass:: OrgBaseNode

   .. automethod:: __init__

.. autoclass:: OrgRootNode

.. autoclass:: OrgNode

.. autoclass:: OrgEnv


Date interface
==============

.. py:module:: orgmodify.date

.. inheritance-diagram::
   orgmodify.date.OrgDate
   orgmodify.date.OrgDateSDCBase
   orgmodify.date.OrgDateScheduled
   orgmodify.date.OrgDateDeadline
   orgmodify.date.OrgDateClosed
   orgmodify.date.OrgDateClock
   orgmodify.date.OrgDateRepeatedTask
   :parts: 1

.. autoclass:: OrgDate

   .. automethod:: __init__

.. autoclass:: OrgDateScheduled
.. autoclass:: OrgDateDeadline
.. autoclass:: OrgDateClosed
.. autoclass:: OrgDateClock
.. autoclass:: OrgDateRepeatedTask


Further resources
=================

.. toctree::

   dev

- `GitHub repository <https://github.com/tkf/orgmodify>`_


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
