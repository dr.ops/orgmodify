Internals
=========

orgmodify.node
-------------

.. py:currentmodule:: orgmodify.node

.. automodule:: orgmodify.node
   :members:
   :private-members:
   :noindex:

orgmodify.date
-------------

.. py:currentmodule:: orgmodify.date

.. automodule:: orgmodify.date
   :members:
   :private-members:
   :noindex:

.. autoclass:: OrgDate
   :members:
   :private-members:
   :noindex:

   .. autoattribute:: OrgDate._active_default
